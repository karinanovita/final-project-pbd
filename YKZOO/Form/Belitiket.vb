﻿Imports iTextSharp.text ' import namespaces .net pdf library
Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Data.OleDb
Public Class Belitiket
    Public username As String = MainMenu.username
    Dim baris, modeProses As Integer
    Dim getBayar As Integer
    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Me.MdiParent = MenuUtama
        TxtKodeMember.Focus()
        loginform.Close()
        CmbKategori.Items.Add("REGUL")
        CmbKategori.Items.Add("PREMI")
        CmbKategori.SelectedIndex = 0
        Txtkode.Text = KontrolTransaksi.kodeTransaksi
        Txtkode.Enabled = False
        picBersih.Enabled = False
        TxtKodeMember.Enabled = False
        LabelAdmin.Text = username
        txtLokasiSave.Enabled = False
        modeProses = 1
        If txtLokasiSave.Text = "" Then
            jikaLokasiKosong(False)
        Else
            jikaLokasiKosong(True)
        End If
    End Sub
    Private Sub txtBayar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBayar.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
    Private Sub txtJumlah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtJmlTiket.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
    Private Sub txtUsia_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtUsia.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Sub TampilGrid()
        DTT = KontrolTransaksi.ReadTransaksi(Txtkode.Text).ToTable
        DGhasil.DataSource = DTT
        If DTT.Rows.Count > 0 Then
            baris = DTT.Rows.Count - 1
            DGhasil.Rows(DTT.Rows.Count - 1).Selected = True
            DGhasil.CurrentCell = DGhasil.Item(1, baris)

        End If
        bayarFunc()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub


    Private Sub BtnProses_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Txtkode_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub CmbKategori_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub BtnBersih_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs)
        FormMember.Show()

    End Sub

    Private Sub TxtKodeMember_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TxtUsia_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub picBack_Click(sender As Object, e As EventArgs) Handles picBack.Click
        Me.Hide()
        MainMenu.Show()
    End Sub

    Private Sub picCetak_Click(sender As Object, e As EventArgs) Handles picCetak.Click
        proses()
    End Sub
    Private Sub proses()
        If TxtNama.Text = "" Or TxtUsia.Text = "" Or TxtJmlTiket.Text = "" Or CmbKategori.Text = "" Or txtBayar.Text = "" Then
            MsgBox("Semua Form Wajib Diisi !")
        Else
            If txtBayar.Text < 25000 * TxtJmlTiket.Text Or txtBayar.Text < 50000 * TxtJmlTiket.Text Then
                MsgBox("Uang Kurang !")
                txtBayar.Focus()
            ElseIf TxtJmlTiket.Text = 0 Then
                MsgBox("Masukkan Jumlah Tiket Yang Benar !")
            Else
                Dim KodeTiket As String
                KodeTiket = CmbKategori.Text
                With EntitasTransaksi
                    .kodeTransaksi = Txtkode.Text
                    .namaPelanggan = TxtNama.Text
                    .kodeMember = TxtKodeMember.Text
                    .jmlTiketTransaksi = TxtJmlTiket.Text
                    .kodeTiket = KodeTiket
                    .tanggalTransaksi = DateTime.Now.ToString("yyyy-MM-dd")
                    .Username1 = LabelAdmin.Text
                End With
                If modeProses = 1 Then
                    KontrolTransaksi.InsertTransaksi(EntitasTransaksi)
                ElseIf modeProses = 2 Then
                    KontrolTransaksi.UpdateTransaksi(EntitasTransaksi)
                End If
                MsgBox("Pembelian Berhasil", MsgBoxStyle.Information, "Info")
                SaveFileDialog1.FileName = ""
                SaveFileDialog1.Filter = "PDF (*.pdf)|*.pdf"
                TxtJmlTiket.Enabled = False
                Txtkode.Enabled = False
                TxtKodeMember.Enabled = False
                TxtNama.Enabled = False
                txtBayar.Enabled = False
                TxtUsia.Enabled = False
                CmbKategori.Enabled = False
                picCetak.Enabled = False
                picBersih.Enabled = True
                TampilGrid()
                bayarFunc()
                cetak()
            End If
        End If

    End Sub

    Private Sub picBersih_Click(sender As Object, e As EventArgs) Handles picBersih.Click
        Txtkode.Text = KontrolTransaksi.kodeTransaksi
        TxtKodeMember.Text = ""
        TxtJmlTiket.Text = ""
        TxtNama.Text = ""
        TxtUsia.Text = ""
        TxtKodeMember.Enabled = False
        txtBayar.Text = ""
        labelKembalian.Text = ""
        CmbKategori.Text = ""
        TxtJmlTiket.Enabled = True
        Txtkode.Enabled = False
        TxtKodeMember.Enabled = True
        TxtNama.Enabled = True
        txtBayar.Enabled = True
        TxtUsia.Enabled = True
        CmbKategori.Enabled = True
        picCetak.Enabled = True
        DTT.Clear()
    End Sub

    Private Sub btMember_Click(sender As Object, e As EventArgs) Handles btMember.Click
        FormMember.Show()
    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub

    Private Sub txtBayar_TextChanged(sender As Object, e As EventArgs) Handles txtBayar.TextChanged

    End Sub

    Private Sub labelKembalian_Click(sender As Object, e As EventArgs) Handles labelKembalian.Click

    End Sub

    Private Sub cetak()
        ' you must import itextsharp namespace into our form
        ' download links is available in the descriptions
        Dim Paragraph As New Paragraph ' declaration for new paragraph F:\YKZOO\OFFLINE\
        Dim PdfFile As New Document(PageSize.A4, 40, 40, 40, 20) ' set pdf page size
        PdfFile.AddTitle(Txtkode.Text) ' set our pdf title
        Dim Write As PdfWriter = PdfWriter.GetInstance(PdfFile, New FileStream(txtLokasiSave.Text & "OFFLINE" + Txtkode.Text & ".pdf", FileMode.Create))
        PdfFile.Open()

        ' declaration font type
        Dim pTitle As New Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim pTable As New Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        ' insert title into pdf file
        Paragraph = New Paragraph(New Chunk(Txtkode.Text, pTitle))
        Paragraph.Alignment = Element.ALIGN_CENTER
        Paragraph.SpacingAfter = 5.0F

        ' set and add page with current settings
        PdfFile.Add(Paragraph)

        ' create data into table
        Dim PdfTable As New PdfPTable(DGhasil.Columns.Count)
        ' setting width of table
        PdfTable.TotalWidth = 500.0F
        PdfTable.LockedWidth = True

        Dim widths(0 To DGhasil.Columns.Count - 1) As Single
        For i As Integer = 0 To DGhasil.Columns.Count - 1
            widths(i) = 1.0F
        Next

        PdfTable.SetWidths(widths)
        PdfTable.HorizontalAlignment = 0
        PdfTable.SpacingBefore = 5.0F

        ' declaration pdf cells
        Dim pdfcell As PdfPCell = New PdfPCell

        ' create pdf header
        For i As Integer = 0 To DGhasil.Columns.Count - 1

            pdfcell = New PdfPCell(New Phrase(New Chunk(DGhasil.Columns(i).HeaderText, pTable)))
            ' alignment header table
            pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT
            ' add cells into pdf table
            PdfTable.AddCell(pdfcell)
        Next

        ' add data into pdf table
        For i As Integer = 0 To DGhasil.Rows.Count - 2

            For j As Integer = 0 To DGhasil.Columns.Count - 1
                pdfcell = New PdfPCell(New Phrase(DGhasil(j, i).Value.ToString(), pTable))
                PdfTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT
                PdfTable.AddCell(pdfcell)
            Next
        Next
        ' add pdf table into pdf document
        PdfFile.Add(PdfTable)
        PdfFile.Close() ' close all sessions

        ' show message if hasben exported
        Dim dialogResult As DialogResult
        dialogResult = MessageBox.Show("Tiket Berhasil Dicetak! Klik OK Untuk Membukannya", "Informations", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If (dialogResult = DialogResult.OK) Then
            Process.Start("explorer.exe", String.Format("/n, /e, {0}", txtLokasiSave.Text & "OFFLINE" + Txtkode.Text & ".pdf"))

        Else

        End If
        labelKembalian.Text = ("Rp. ") + (txtBayar.Text - getBayar).ToString

    End Sub
    Private Sub DGHasil_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGhasil.CellContentClick
        Dim newBaris As Integer
        newBaris = e.RowIndex
        DGhasil.Rows(0).Selected = True

    End Sub

    Private Sub picProses_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub CmbKategori_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles CmbKategori.SelectedIndexChanged

    End Sub

    Public Sub bayarFunc()

        If 0 < DTT.Rows.Count Then
            With DGhasil.Rows(0)
                getBayar = .Cells(7).Value.ToString
            End With
        End If

    End Sub

    Private Sub SaveFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk

    End Sub

    Private Sub BbtBrowse_Click(sender As Object, e As EventArgs) Handles BbtBrowse.Click
        Dim folderDlg As New FolderBrowserDialog

        folderDlg.ShowNewFolderButton = True

        If (folderDlg.ShowDialog() = DialogResult.OK) Then

            txtLokasiSave.Text = folderDlg.SelectedPath + "\"
            If txtLokasiSave.Text <> "" Then
                jikaLokasiKosong(True)
            Else
                jikaLokasiKosong(False)
            End If

            Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If
    End Sub

    Private Sub txtBayar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBayar.KeyDown
        If e.KeyCode = Keys.Enter Then
            proses()
        End If
    End Sub

    Private Sub Label11_Click(sender As Object, e As EventArgs) Handles Label11.Click

    End Sub

    Private Sub jikaLokasiKosong(yn As Boolean)
        TxtNama.Enabled = yn
        txtBayar.Enabled = yn
        TxtJmlTiket.Enabled = yn
        TxtKodeMember.Enabled = yn
        CmbKategori.Enabled = yn
        btMember.Enabled = yn
        TxtUsia.Enabled = yn
        picCetak.Enabled = yn
        picBersih.Enabled = yn

    End Sub
End Class

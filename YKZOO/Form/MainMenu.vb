﻿Public Class MainMenu
    Public username As String
    Private Sub btTiketOffline_Click(sender As Object, e As EventArgs)
        Me.Hide()
        Belitiket.Show()
    End Sub

    Private Sub btTiketOnline_Click(sender As Object, e As EventArgs)
        Me.Hide()
        BeliTiketOnline.Show()
    End Sub

    Private Sub btTransaksi_Click(sender As Object, e As EventArgs)
        Me.Hide()
        dtTransaksi.Show()

    End Sub

    Private Sub btLogout_Click(sender As Object, e As EventArgs)
        Me.Hide()
        loginform.Show()

    End Sub

    Private Sub MainMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LabelAdmin.Text = loginform.username
        username = LabelAdmin.Text
    End Sub

    Private Sub offline_Click(sender As Object, e As EventArgs) Handles logout.Click
        Me.Hide()
        loginform.Show()

    End Sub

    Private Sub transaksi_Click(sender As Object, e As EventArgs) Handles transaksi.Click
        Me.Hide()
        dtTransaksi.Show()
    End Sub

    Private Sub tiketoff_Click(sender As Object, e As EventArgs) Handles tiketoff.Click
        Me.Hide()
        Belitiket.Show()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Me.Hide()
        BeliTiketOnline.Show()
    End Sub

    Private Sub picMember_Click(sender As Object, e As EventArgs) Handles picMember.Click
        AddMember.Show()
    End Sub
End Class
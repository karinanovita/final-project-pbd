﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class loginform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(loginform))
        Me.labelPasswd = New System.Windows.Forms.Label()
        Me.labelidKasir = New System.Windows.Forms.Label()
        Me.TxtPassword = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Txtusername = New System.Windows.Forms.TextBox()
        Me.picLogin = New System.Windows.Forms.PictureBox()
        CType(Me.picLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'labelPasswd
        '
        Me.labelPasswd.AutoSize = True
        Me.labelPasswd.BackColor = System.Drawing.Color.Transparent
        Me.labelPasswd.Location = New System.Drawing.Point(100, 176)
        Me.labelPasswd.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.labelPasswd.Name = "labelPasswd"
        Me.labelPasswd.Size = New System.Drawing.Size(78, 20)
        Me.labelPasswd.TabIndex = 3
        Me.labelPasswd.Text = "Password"
        '
        'labelidKasir
        '
        Me.labelidKasir.AutoSize = True
        Me.labelidKasir.BackColor = System.Drawing.Color.Transparent
        Me.labelidKasir.Location = New System.Drawing.Point(116, 141)
        Me.labelidKasir.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.labelidKasir.Name = "labelidKasir"
        Me.labelidKasir.Size = New System.Drawing.Size(62, 20)
        Me.labelidKasir.TabIndex = 2
        Me.labelidKasir.Text = "Id Kasir"
        '
        'TxtPassword
        '
        Me.TxtPassword.Location = New System.Drawing.Point(250, 176)
        Me.TxtPassword.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtPassword.Name = "TxtPassword"
        Me.TxtPassword.Size = New System.Drawing.Size(338, 26)
        Me.TxtPassword.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(369, 83)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 37)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "LOGIN"
        '
        'Txtusername
        '
        Me.Txtusername.Location = New System.Drawing.Point(250, 141)
        Me.Txtusername.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Txtusername.Name = "Txtusername"
        Me.Txtusername.Size = New System.Drawing.Size(338, 26)
        Me.Txtusername.TabIndex = 1
        '
        'picLogin
        '
        Me.picLogin.BackgroundImage = CType(resources.GetObject("picLogin.BackgroundImage"), System.Drawing.Image)
        Me.picLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picLogin.Location = New System.Drawing.Point(622, 141)
        Me.picLogin.Name = "picLogin"
        Me.picLogin.Size = New System.Drawing.Size(60, 61)
        Me.picLogin.TabIndex = 5
        Me.picLogin.TabStop = False
        '
        'loginform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(801, 330)
        Me.Controls.Add(Me.picLogin)
        Me.Controls.Add(Me.Txtusername)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.labelPasswd)
        Me.Controls.Add(Me.TxtPassword)
        Me.Controls.Add(Me.labelidKasir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "loginform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        CType(Me.picLogin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents labelPasswd As Label
    Friend WithEvents labelidKasir As Label
    Friend WithEvents TxtPassword As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Txtusername As TextBox
    Friend WithEvents picLogin As PictureBox
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dtTransaksi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(dtTransaksi))
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.labelPendapatan = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.totalPengunjung = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.picCetak = New System.Windows.Forms.PictureBox()
        Me.picDelete = New System.Windows.Forms.PictureBox()
        Me.picBack = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCariData = New System.Windows.Forms.TextBox()
        Me.picCari = New System.Windows.Forms.PictureBox()
        Me.txtLokasiSave = New System.Windows.Forms.TextBox()
        Me.BbtBrowse = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCetak, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBack, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCari, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'labelPendapatan
        '
        Me.labelPendapatan.AutoSize = True
        Me.labelPendapatan.Location = New System.Drawing.Point(576, 444)
        Me.labelPendapatan.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.labelPendapatan.Name = "labelPendapatan"
        Me.labelPendapatan.Size = New System.Drawing.Size(18, 20)
        Me.labelPendapatan.TabIndex = 16
        Me.labelPendapatan.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(472, 444)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 20)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Pendapatan :"
        '
        'totalPengunjung
        '
        Me.totalPengunjung.AutoSize = True
        Me.totalPengunjung.Location = New System.Drawing.Point(279, 444)
        Me.totalPengunjung.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.totalPengunjung.Name = "totalPengunjung"
        Me.totalPengunjung.Size = New System.Drawing.Size(18, 20)
        Me.totalPengunjung.TabIndex = 14
        Me.totalPengunjung.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(130, 444)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(141, 20)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Total Pengunjung :"
        '
        'DataGridView1
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightSteelBlue
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.GreenYellow
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(62, 86)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(901, 353)
        Me.DataGridView1.TabIndex = 11
        '
        'picCetak
        '
        Me.picCetak.BackgroundImage = CType(resources.GetObject("picCetak.BackgroundImage"), System.Drawing.Image)
        Me.picCetak.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picCetak.Location = New System.Drawing.Point(600, 500)
        Me.picCetak.Name = "picCetak"
        Me.picCetak.Size = New System.Drawing.Size(73, 83)
        Me.picCetak.TabIndex = 19
        Me.picCetak.TabStop = False
        '
        'picDelete
        '
        Me.picDelete.BackgroundImage = CType(resources.GetObject("picDelete.BackgroundImage"), System.Drawing.Image)
        Me.picDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picDelete.Location = New System.Drawing.Point(510, 500)
        Me.picDelete.Name = "picDelete"
        Me.picDelete.Size = New System.Drawing.Size(84, 83)
        Me.picDelete.TabIndex = 20
        Me.picDelete.TabStop = False
        '
        'picBack
        '
        Me.picBack.BackgroundImage = CType(resources.GetObject("picBack.BackgroundImage"), System.Drawing.Image)
        Me.picBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picBack.Location = New System.Drawing.Point(416, 500)
        Me.picBack.Name = "picBack"
        Me.picBack.Size = New System.Drawing.Size(88, 83)
        Me.picBack.TabIndex = 21
        Me.picBack.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(58, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 20)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Cari :"
        '
        'txtCariData
        '
        Me.txtCariData.Location = New System.Drawing.Point(109, 47)
        Me.txtCariData.Name = "txtCariData"
        Me.txtCariData.Size = New System.Drawing.Size(395, 26)
        Me.txtCariData.TabIndex = 25
        '
        'picCari
        '
        Me.picCari.BackgroundImage = CType(resources.GetObject("picCari.BackgroundImage"), System.Drawing.Image)
        Me.picCari.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picCari.Location = New System.Drawing.Point(510, 42)
        Me.picCari.Name = "picCari"
        Me.picCari.Size = New System.Drawing.Size(37, 36)
        Me.picCari.TabIndex = 26
        Me.picCari.TabStop = False
        '
        'txtLokasiSave
        '
        Me.txtLokasiSave.Location = New System.Drawing.Point(755, 551)
        Me.txtLokasiSave.Name = "txtLokasiSave"
        Me.txtLokasiSave.Size = New System.Drawing.Size(166, 26)
        Me.txtLokasiSave.TabIndex = 60
        '
        'BbtBrowse
        '
        Me.BbtBrowse.BackColor = System.Drawing.Color.DarkBlue
        Me.BbtBrowse.ForeColor = System.Drawing.Color.White
        Me.BbtBrowse.Location = New System.Drawing.Point(930, 545)
        Me.BbtBrowse.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.BbtBrowse.Name = "BbtBrowse"
        Me.BbtBrowse.Size = New System.Drawing.Size(107, 38)
        Me.BbtBrowse.TabIndex = 59
        Me.BbtBrowse.Text = "Browse..."
        Me.BbtBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BbtBrowse.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(751, 511)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(284, 20)
        Me.Label1.TabIndex = 61
        Me.Label1.Text = "*tentukan lokasi simpan sebelum cetak"
        '
        'dtTransaksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(1050, 643)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtLokasiSave)
        Me.Controls.Add(Me.BbtBrowse)
        Me.Controls.Add(Me.picCari)
        Me.Controls.Add(Me.txtCariData)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.picBack)
        Me.Controls.Add(Me.picDelete)
        Me.Controls.Add(Me.picCetak)
        Me.Controls.Add(Me.labelPendapatan)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.totalPengunjung)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DataGridView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "dtTransaksi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "dtTransaksi"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCetak, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBack, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCari, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents labelPendapatan As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents totalPengunjung As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents picCetak As PictureBox
    Friend WithEvents picDelete As PictureBox
    Friend WithEvents picBack As PictureBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtCariData As TextBox
    Friend WithEvents picCari As PictureBox
    Friend WithEvents txtLokasiSave As TextBox
    Friend WithEvents BbtBrowse As Button
    Friend WithEvents Label1 As Label
End Class

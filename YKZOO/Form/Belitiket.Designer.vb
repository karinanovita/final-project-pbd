﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Belitiket
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Belitiket))
        Me.btMember = New System.Windows.Forms.Button()
        Me.TxtKodeMember = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Txtkode = New System.Windows.Forms.TextBox()
        Me.DGhasil = New System.Windows.Forms.DataGridView()
        Me.CmbKategori = New System.Windows.Forms.ComboBox()
        Me.TxtJmlTiket = New System.Windows.Forms.TextBox()
        Me.TxtUsia = New System.Windows.Forms.TextBox()
        Me.TxtNama = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.picCetak = New System.Windows.Forms.PictureBox()
        Me.picBersih = New System.Windows.Forms.PictureBox()
        Me.picBack = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.labelKembalian = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtBayar = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LabelAdmin = New System.Windows.Forms.Label()
        Me.txtLokasiSave = New System.Windows.Forms.TextBox()
        Me.BbtBrowse = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        CType(Me.DGhasil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCetak, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBersih, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btMember
        '
        Me.btMember.BackColor = System.Drawing.Color.DarkBlue
        Me.btMember.ForeColor = System.Drawing.Color.White
        Me.btMember.Location = New System.Drawing.Point(433, 74)
        Me.btMember.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btMember.Name = "btMember"
        Me.btMember.Size = New System.Drawing.Size(107, 38)
        Me.btMember.TabIndex = 45
        Me.btMember.Text = ">>"
        Me.btMember.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btMember.UseVisualStyleBackColor = False
        '
        'TxtKodeMember
        '
        Me.TxtKodeMember.Location = New System.Drawing.Point(200, 80)
        Me.TxtKodeMember.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtKodeMember.Name = "TxtKodeMember"
        Me.TxtKodeMember.Size = New System.Drawing.Size(225, 26)
        Me.TxtKodeMember.TabIndex = 44
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(89, 82)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 20)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Kode Member"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(141, 117)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 20)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Nama"
        '
        'Txtkode
        '
        Me.Txtkode.Location = New System.Drawing.Point(200, 39)
        Me.Txtkode.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Txtkode.Name = "Txtkode"
        Me.Txtkode.Size = New System.Drawing.Size(225, 26)
        Me.Txtkode.TabIndex = 41
        '
        'DGhasil
        '
        Me.DGhasil.BackgroundColor = System.Drawing.Color.GreenYellow
        Me.DGhasil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGhasil.GridColor = System.Drawing.Color.GreenYellow
        Me.DGhasil.Location = New System.Drawing.Point(3, 370)
        Me.DGhasil.Name = "DGhasil"
        Me.DGhasil.RowTemplate.Height = 28
        Me.DGhasil.Size = New System.Drawing.Size(1045, 103)
        Me.DGhasil.TabIndex = 40
        '
        'CmbKategori
        '
        Me.CmbKategori.FormattingEnabled = True
        Me.CmbKategori.Location = New System.Drawing.Point(200, 185)
        Me.CmbKategori.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.CmbKategori.Name = "CmbKategori"
        Me.CmbKategori.Size = New System.Drawing.Size(225, 28)
        Me.CmbKategori.TabIndex = 37
        '
        'TxtJmlTiket
        '
        Me.TxtJmlTiket.Location = New System.Drawing.Point(200, 223)
        Me.TxtJmlTiket.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtJmlTiket.Name = "TxtJmlTiket"
        Me.TxtJmlTiket.Size = New System.Drawing.Size(225, 26)
        Me.TxtJmlTiket.TabIndex = 36
        '
        'TxtUsia
        '
        Me.TxtUsia.Location = New System.Drawing.Point(200, 152)
        Me.TxtUsia.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtUsia.Name = "TxtUsia"
        Me.TxtUsia.Size = New System.Drawing.Size(225, 26)
        Me.TxtUsia.TabIndex = 35
        '
        'TxtNama
        '
        Me.TxtNama.Location = New System.Drawing.Point(200, 116)
        Me.TxtNama.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtNama.Name = "TxtNama"
        Me.TxtNama.Size = New System.Drawing.Size(225, 26)
        Me.TxtNama.TabIndex = 34
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(500, 157)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(0, 20)
        Me.Label7.TabIndex = 33
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(124, 193)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 20)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Kategori"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(99, 226)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 20)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Jumlah Tiket"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(151, 152)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 20)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Usia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(113, 43)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 20)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Kode Tiket"
        '
        'SaveFileDialog1
        '
        '
        'picCetak
        '
        Me.picCetak.BackgroundImage = CType(resources.GetObject("picCetak.BackgroundImage"), System.Drawing.Image)
        Me.picCetak.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picCetak.Location = New System.Drawing.Point(237, 297)
        Me.picCetak.Name = "picCetak"
        Me.picCetak.Size = New System.Drawing.Size(64, 61)
        Me.picCetak.TabIndex = 46
        Me.picCetak.TabStop = False
        '
        'picBersih
        '
        Me.picBersih.BackgroundImage = CType(resources.GetObject("picBersih.BackgroundImage"), System.Drawing.Image)
        Me.picBersih.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picBersih.Location = New System.Drawing.Point(321, 297)
        Me.picBersih.Name = "picBersih"
        Me.picBersih.Size = New System.Drawing.Size(64, 61)
        Me.picBersih.TabIndex = 47
        Me.picBersih.TabStop = False
        '
        'picBack
        '
        Me.picBack.BackgroundImage = CType(resources.GetObject("picBack.BackgroundImage"), System.Drawing.Image)
        Me.picBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picBack.Location = New System.Drawing.Point(48, 513)
        Me.picBack.Name = "picBack"
        Me.picBack.Size = New System.Drawing.Size(100, 85)
        Me.picBack.TabIndex = 48
        Me.picBack.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.labelKembalian)
        Me.Panel1.Location = New System.Drawing.Point(780, 80)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(268, 100)
        Me.Panel1.TabIndex = 49
        '
        'labelKembalian
        '
        Me.labelKembalian.AutoSize = True
        Me.labelKembalian.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelKembalian.Location = New System.Drawing.Point(26, 43)
        Me.labelKembalian.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.labelKembalian.Name = "labelKembalian"
        Me.labelKembalian.Size = New System.Drawing.Size(23, 25)
        Me.labelKembalian.TabIndex = 51
        Me.labelKembalian.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label8.Location = New System.Drawing.Point(585, 117)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(165, 25)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Total Kembalian :"
        '
        'txtBayar
        '
        Me.txtBayar.Location = New System.Drawing.Point(200, 263)
        Me.txtBayar.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtBayar.Name = "txtBayar"
        Me.txtBayar.Size = New System.Drawing.Size(225, 26)
        Me.txtBayar.TabIndex = 52
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(103, 266)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 20)
        Me.Label9.TabIndex = 51
        Me.Label9.Text = "Uang Bayar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label10.Location = New System.Drawing.Point(806, 19)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 25)
        Me.Label10.TabIndex = 53
        Me.Label10.Text = "Admin :"
        '
        'LabelAdmin
        '
        Me.LabelAdmin.AutoSize = True
        Me.LabelAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LabelAdmin.Location = New System.Drawing.Point(893, 19)
        Me.LabelAdmin.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelAdmin.Name = "LabelAdmin"
        Me.LabelAdmin.Size = New System.Drawing.Size(60, 25)
        Me.LabelAdmin.TabIndex = 54
        Me.LabelAdmin.Text = "gustu"
        '
        'txtLokasiSave
        '
        Me.txtLokasiSave.Location = New System.Drawing.Point(664, 583)
        Me.txtLokasiSave.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtLokasiSave.Name = "txtLokasiSave"
        Me.txtLokasiSave.Size = New System.Drawing.Size(226, 26)
        Me.txtLokasiSave.TabIndex = 55
        '
        'BbtBrowse
        '
        Me.BbtBrowse.BackColor = System.Drawing.Color.DarkBlue
        Me.BbtBrowse.ForeColor = System.Drawing.Color.White
        Me.BbtBrowse.Location = New System.Drawing.Point(898, 577)
        Me.BbtBrowse.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.BbtBrowse.Name = "BbtBrowse"
        Me.BbtBrowse.Size = New System.Drawing.Size(107, 38)
        Me.BbtBrowse.TabIndex = 56
        Me.BbtBrowse.Text = "Browse..."
        Me.BbtBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BbtBrowse.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(586, 542)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(455, 20)
        Me.Label11.TabIndex = 57
        Me.Label11.Text = "*sebelum bertransaksi harap pilih direktori untuk menyimpan file"
        '
        'Belitiket
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(1050, 643)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.BbtBrowse)
        Me.Controls.Add(Me.txtLokasiSave)
        Me.Controls.Add(Me.LabelAdmin)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtBayar)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.picBack)
        Me.Controls.Add(Me.picBersih)
        Me.Controls.Add(Me.picCetak)
        Me.Controls.Add(Me.btMember)
        Me.Controls.Add(Me.TxtKodeMember)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Txtkode)
        Me.Controls.Add(Me.DGhasil)
        Me.Controls.Add(Me.CmbKategori)
        Me.Controls.Add(Me.TxtJmlTiket)
        Me.Controls.Add(Me.TxtUsia)
        Me.Controls.Add(Me.TxtNama)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Belitiket"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Kasir Tiket YK ZOO"
        CType(Me.DGhasil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCetak, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBersih, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btMember As Button
    Friend WithEvents TxtKodeMember As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Txtkode As TextBox
    Friend WithEvents DGhasil As DataGridView
    Friend WithEvents CmbKategori As ComboBox
    Friend WithEvents TxtJmlTiket As TextBox
    Friend WithEvents TxtUsia As TextBox
    Friend WithEvents TxtNama As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents picCetak As PictureBox
    Friend WithEvents picBersih As PictureBox
    Friend WithEvents picBack As PictureBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents labelKembalian As Label
    Friend WithEvents txtBayar As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents LabelAdmin As Label
    Friend WithEvents txtLokasiSave As TextBox
    Friend WithEvents BbtBrowse As Button
    Friend WithEvents Label11 As Label
End Class

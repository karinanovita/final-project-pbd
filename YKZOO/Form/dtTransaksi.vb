﻿Imports System.Data.Odbc ' import namespaces ODBC class
Imports iTextSharp.text ' import namespaces .net pdf library
Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Data.OleDb

Public Class dtTransaksi
    Dim kodeMember As String
    Dim posisi, baris As Integer
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getData()
        CountPendapatan()
        CountPengunjung()
        txtLokasiSave.Enabled = False
        If txtLokasiSave.Text = "" Then
            jikaLokasiKosong(False)
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub



    Private Sub SaveFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        posisi = e.RowIndex
        DataGridView1.Rows(posisi).Selected = True
        sendMember(posisi)
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs)

    End Sub
    Private Sub CountPengunjung()
        OPENKONEKSI()
        Dim query As String = "SELECT COUNT (kd_transaksi) AS PENGUNJUNG FROM TRANSAKSI WHERE kd_transaksi !='B0001'"
        If New OleDbCommand(query, OPENKONEKSI).ExecuteReader.HasRows Then
            CMD = New OleDbCommand(query, OPENKONEKSI)
            DTR = CMD.ExecuteReader
            DTR.Read()
            If DTR.HasRows Then
                totalPengunjung.Text = DTR.GetValue(0).ToString
            End If
        End If
    End Sub
    Private Sub CountPendapatan()
        OPENKONEKSI()
        Dim query As String = "SELECT SUM (totalBayar) AS PENDAPATAN FROM TRANSAKSI WHERE kd_transaksi !='B0001'"
        If New OleDbCommand(query, OPENKONEKSI).ExecuteReader.HasRows Then
            CMD = New OleDbCommand(query, OPENKONEKSI)
            DTR = CMD.ExecuteReader
            DTR.Read()
            If DTR.HasRows Then
                labelPendapatan.Text = DTR.GetValue(0).ToString
            End If
        End If
    End Sub
    Private Sub deleteDataTransaksi()

        OPENKONEKSI()
        Dim queryDetailTrans As String = "DELETE Transaksi WHERE kd_transaksi !='B0001'"
        If New OleDbCommand(queryDetailTrans, koneksi).ExecuteReader.HasRows = True Then
            CMD = New OleDbCommand(queryDetailTrans, koneksi)
            DTR = CMD.ExecuteReader
            DTR.Read()
            If DTR.HasRows Then
                OPENKONEKSI()
                DTA = New OleDbDataAdapter(queryDetailTrans, koneksi)
                DTS = New DataSet
                DTA.Fill(DTS, "data_ketemu")
                DataGridView1.DataSource = DTS.Tables("data_ketemu")
                DataGridView1.ReadOnly = True
                SaveFileDialog1.FileName = ""
                SaveFileDialog1.Filter = "PDF (*.pdf)|*.pdf"

            Else
                DataGridView1.Rows(0).Cells(0).Value = "Data Kosong"
            End If
        End If
    End Sub
    Private Sub deleteDataTransaksiSelect(kode As String)

        OPENKONEKSI()
        Dim queryDetailTrans As String = "DELETE Transaksi WHERE kd_transaksi ='" & kode & "'"
        If New OleDbCommand(queryDetailTrans, koneksi).ExecuteReader.HasRows = True Then
            CMD = New OleDbCommand(queryDetailTrans, koneksi)
            DTR = CMD.ExecuteReader

        End If
    End Sub
    Public Function selectMember() As DataView
        Dim Data As New ClsEntitasMember
        Try
            DTA = New OleDbDataAdapter("Select * From Transaksi WHERE kd_transaksi!='B0001'", OPENKONEKSI)
            DTS = New DataSet
            DTA.Fill(DTS, "tabel_transaksi")
            Dim grid As New DataView(DTS.Tables("tabel_transaksi"))
            Return grid

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Private Sub getData()
        DTT = selectMember.ToTable
        DataGridView1.DataSource = DTT
        If DTT.Rows.Count > 0 Then
            baris = DTT.Rows.Count - 1
            DataGridView1.Rows(DTT.Rows.Count - 1).Selected = True
            DataGridView1.CurrentCell = DataGridView1.Item(1, baris)

        End If
        'configuration fo save file dialog
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.Filter = "PDF (*.pdf)|*.pdf"
    End Sub
    Private Sub btDelete_Click(sender As Object, e As EventArgs)
        Dim dr As DialogResult
        dr = MessageBox.Show("Apakah Anda Yakin Ingin Menghapus Semua Data Transaksi?", "Peringatan", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)

        If (dr = DialogResult.OK) Then
            deleteDataTransaksi()
            getData()
            MsgBox("Data Berhasil Dihapus")

        Else

        End If

    End Sub

    Private Sub btBack_Click(sender As Object, e As EventArgs)
        MainMenu.Show()
        Me.Hide()
    End Sub

    Private Sub picCetak_Click(sender As Object, e As EventArgs) Handles picCetak.Click
        ' you must import itextsharp namespace into our form
        ' download links is available in the descriptions
        Dim Paragraph As New Paragraph ' declaration for new paragraph
        Dim PdfFile As New Document(PageSize.A4, 40, 40, 40, 20) ' set pdf page size
        PdfFile.AddTitle("LAPORAN TRANSAKSI") ' set our pdf title
        Dim Write As PdfWriter = PdfWriter.GetInstance(PdfFile, New FileStream(txtLokasiSave.Text & "LAPORAN TRANSAKSI.pdf", FileMode.Create))
        PdfFile.Open()

        ' declaration font type
        Dim pTitle As New Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim pTable As New Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        ' insert title into pdf file
        Paragraph = New Paragraph(New Chunk("LAPORAN TRANSAKSI", pTitle))
        Paragraph.Alignment = Element.ALIGN_CENTER
        Paragraph.SpacingAfter = 5.0F

        ' set and add page with current settings
        PdfFile.Add(Paragraph)

        ' create data into table
        Dim PdfTable As New PdfPTable(DataGridView1.Columns.Count)
        ' setting width of table
        PdfTable.TotalWidth = 500.0F
        PdfTable.LockedWidth = True

        Dim widths(0 To DataGridView1.Columns.Count - 1) As Single
        For i As Integer = 0 To DataGridView1.Columns.Count - 1
            widths(i) = 1.0F
        Next

        PdfTable.SetWidths(widths)
        PdfTable.HorizontalAlignment = 0
        PdfTable.SpacingBefore = 5.0F

        ' declaration pdf cells
        Dim pdfcell As PdfPCell = New PdfPCell

        ' create pdf header
        For i As Integer = 0 To DataGridView1.Columns.Count - 1

            pdfcell = New PdfPCell(New Phrase(New Chunk(DataGridView1.Columns(i).HeaderText, pTable)))
            ' alignment header table
            pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT
            ' add cells into pdf table
            PdfTable.AddCell(pdfcell)
        Next

        ' add data into pdf table
        For i As Integer = 0 To DataGridView1.Rows.Count - 2

            For j As Integer = 0 To DataGridView1.Columns.Count - 1
                pdfcell = New PdfPCell(New Phrase(DataGridView1(j, i).Value.ToString(), pTable))
                PdfTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT
                PdfTable.AddCell(pdfcell)
            Next
        Next
        ' add pdf table into pdf document
        PdfFile.Add(PdfTable)
        PdfFile.Close() ' close all sessions

        ' show message if hasben exported
        Dim dialogResult As DialogResult
        dialogResult = MessageBox.Show("Detail Transaksi Berhasil Dicetak di " & txtLokasiSave.Text & "" & "LAPORAN TRANSAKSI.pdf ", "Informations", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If (dialogResult = DialogResult.OK) Then
            Process.Start("explorer.exe", String.Format("/n, /e, {0}", txtLokasiSave.Text & "LAPORAN TRANSAKSI.pdf"))
        Else

        End If
    End Sub

    Private Sub picDelete_Click(sender As Object, e As EventArgs) Handles picDelete.Click
        deleteDataTransaksi()

    End Sub

    Private Sub btDelete_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub btBack_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub picBack_Click(sender As Object, e As EventArgs) Handles picBack.Click
        Me.Hide()
        MainMenu.Show()
    End Sub

    Private Sub picCari_Click(sender As Object, e As EventArgs) Handles picCari.Click
        showCari(txtCariData.Text.ToString)
    End Sub
    Private Sub showCari(nama As String)
        OPENKONEKSI()
        Dim query As String = "SELECT * FROM TRANSAKSI WHERE kd_transaksi LIKE'%" & nama & "%' or nama LIKE '%" & nama & "%' "
        If New OleDbCommand(query, koneksi).ExecuteReader().HasRows = True Then
            DataGridView1.Show()

            CMD = New OleDbCommand(query, koneksi)
            DTR = CMD.ExecuteReader
            DTR.Read()
            If DTR.HasRows Then
                OPENKONEKSI()
                DTA = New OleDbDataAdapter(query, koneksi)
                DTS = New DataSet
                DTA.Fill(DTS, "data_ketemu")
                DataGridView1.DataSource = DTS.Tables("data_ketemu")
                DataGridView1.ReadOnly = True

                SaveFileDialog1.FileName = ""
                SaveFileDialog1.Filter = "PDF (*.pdf)|*.pdf"
            End If
        Else

            MsgBox("Pelanggan Tidak Terdaftar!")


        End If

    End Sub
    Private Sub txtCariData_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCariData.KeyDown
        If e.KeyCode = Keys.Enter Then
            showCari(txtCariData.Text.ToString)
        End If
    End Sub

    Private Sub cbFilter_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub DataGridView1_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        sendMember(posisi)
        If posisi < 0 Then

        Else
            If e.KeyCode = Keys.Delete Then
                Dim dialogresult As DialogResult
                dialogresult = MessageBox.Show("Apakah Anda Ingin Menghapus ?", "Peringatan", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
                If dialogresult = DialogResult.OK Then
                    deleteDataTransaksiSelect(kodeMember)
                    Dim okResult As DialogResult
                    okResult = MessageBox.Show("Data Berhasil Dihapus", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If okResult = DialogResult.OK Then
                        getData()
                        CountPengunjung()
                        CountPendapatan()
                    End If

                Else

                End If
            End If
        End If

    End Sub

    Private Sub txtLokasiSave_TextChanged(sender As Object, e As EventArgs) Handles txtLokasiSave.TextChanged

    End Sub

    Private Sub BbtBrowse_Click(sender As Object, e As EventArgs) Handles BbtBrowse.Click
        Dim folderDlg As New FolderBrowserDialog

        folderDlg.ShowNewFolderButton = True

        If (folderDlg.ShowDialog() = DialogResult.OK) Then

            txtLokasiSave.Text = folderDlg.SelectedPath + "\"
            If txtLokasiSave.Text <> "" Then
                jikaLokasiKosong(True)
            Else
                jikaLokasiKosong(False)
            End If

            Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If
    End Sub

    Private Sub sendMember(br As Integer)
        If br < DTT.Rows.Count Then
            With DataGridView1.Rows(br)
                kodeMember = .Cells(0).Value.ToString
            End With
        End If
    End Sub
    Private Sub jikaLokasiKosong(yn As Boolean)
        picCetak.Enabled = yn

    End Sub
End Class

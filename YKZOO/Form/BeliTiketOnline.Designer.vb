﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BeliTiketOnline
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BeliTiketOnline))
        Me.kd_pelanggan = New System.Windows.Forms.TextBox()
        Me.btCekOnline = New System.Windows.Forms.Button()
        Me.DataGridTampilPelangganONLINE = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.print = New System.Windows.Forms.PictureBox()
        Me.BbtBrowse = New System.Windows.Forms.Button()
        Me.txtLokasiSave = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.DataGridTampilPelangganONLINE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.print, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(35, 126)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(131, 20)
        Label1.TabIndex = 1
        Label1.Text = "Masukkan Kode :"
        '
        'kd_pelanggan
        '
        Me.kd_pelanggan.Location = New System.Drawing.Point(39, 149)
        Me.kd_pelanggan.Name = "kd_pelanggan"
        Me.kd_pelanggan.Size = New System.Drawing.Size(424, 26)
        Me.kd_pelanggan.TabIndex = 0
        '
        'btCekOnline
        '
        Me.btCekOnline.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btCekOnline.ForeColor = System.Drawing.Color.White
        Me.btCekOnline.Location = New System.Drawing.Point(469, 146)
        Me.btCekOnline.Name = "btCekOnline"
        Me.btCekOnline.Size = New System.Drawing.Size(75, 32)
        Me.btCekOnline.TabIndex = 4
        Me.btCekOnline.Text = "Cek"
        Me.btCekOnline.UseVisualStyleBackColor = False
        '
        'DataGridTampilPelangganONLINE
        '
        Me.DataGridTampilPelangganONLINE.BackgroundColor = System.Drawing.Color.GreenYellow
        Me.DataGridTampilPelangganONLINE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridTampilPelangganONLINE.Location = New System.Drawing.Point(39, 193)
        Me.DataGridTampilPelangganONLINE.Name = "DataGridTampilPelangganONLINE"
        Me.DataGridTampilPelangganONLINE.RowTemplate.Height = 28
        Me.DataGridTampilPelangganONLINE.Size = New System.Drawing.Size(970, 150)
        Me.DataGridTampilPelangganONLINE.TabIndex = 5
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Blue
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(39, 576)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(161, 55)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Back"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'print
        '
        Me.print.BackgroundImage = CType(resources.GetObject("print.BackgroundImage"), System.Drawing.Image)
        Me.print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.print.Location = New System.Drawing.Point(955, 134)
        Me.print.Name = "print"
        Me.print.Size = New System.Drawing.Size(54, 53)
        Me.print.TabIndex = 8
        Me.print.TabStop = False
        '
        'BbtBrowse
        '
        Me.BbtBrowse.BackColor = System.Drawing.Color.DarkBlue
        Me.BbtBrowse.ForeColor = System.Drawing.Color.White
        Me.BbtBrowse.Location = New System.Drawing.Point(902, 584)
        Me.BbtBrowse.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.BbtBrowse.Name = "BbtBrowse"
        Me.BbtBrowse.Size = New System.Drawing.Size(107, 38)
        Me.BbtBrowse.TabIndex = 57
        Me.BbtBrowse.Text = "Browse..."
        Me.BbtBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BbtBrowse.UseVisualStyleBackColor = False
        '
        'txtLokasiSave
        '
        Me.txtLokasiSave.Location = New System.Drawing.Point(653, 590)
        Me.txtLokasiSave.Name = "txtLokasiSave"
        Me.txtLokasiSave.Size = New System.Drawing.Size(240, 26)
        Me.txtLokasiSave.TabIndex = 58
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(725, 546)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(284, 20)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "*tentukan lokasi simpan sebelum cetak"
        '
        'BeliTiketOnline
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(1050, 643)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtLokasiSave)
        Me.Controls.Add(Me.BbtBrowse)
        Me.Controls.Add(Me.print)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridTampilPelangganONLINE)
        Me.Controls.Add(Me.btCekOnline)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.kd_pelanggan)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "BeliTiketOnline"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BeliTiketOnline"
        CType(Me.DataGridTampilPelangganONLINE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.print, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents kd_pelanggan As TextBox
    Friend WithEvents btCekOnline As Button
    Friend WithEvents DataGridTampilPelangganONLINE As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents print As PictureBox
    Friend WithEvents BbtBrowse As Button
    Friend WithEvents txtLokasiSave As TextBox
    Friend WithEvents Label2 As Label
End Class

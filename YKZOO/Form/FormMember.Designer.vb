﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMember
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMember))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridViewMember = New System.Windows.Forms.DataGridView()
        Me.picCheck = New System.Windows.Forms.PictureBox()
        Me.picAdd = New System.Windows.Forms.PictureBox()
        CType(Me.DataGridViewMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picAdd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(187, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(428, 69)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Daftar Member"
        '
        'DataGridViewMember
        '
        Me.DataGridViewMember.BackgroundColor = System.Drawing.Color.GreenYellow
        Me.DataGridViewMember.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewMember.Location = New System.Drawing.Point(12, 141)
        Me.DataGridViewMember.Name = "DataGridViewMember"
        Me.DataGridViewMember.RowTemplate.Height = 28
        Me.DataGridViewMember.Size = New System.Drawing.Size(776, 156)
        Me.DataGridViewMember.TabIndex = 1
        '
        'picCheck
        '
        Me.picCheck.BackgroundImage = CType(resources.GetObject("picCheck.BackgroundImage"), System.Drawing.Image)
        Me.picCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picCheck.Location = New System.Drawing.Point(309, 324)
        Me.picCheck.Name = "picCheck"
        Me.picCheck.Size = New System.Drawing.Size(84, 79)
        Me.picCheck.TabIndex = 3
        Me.picCheck.TabStop = False
        '
        'picAdd
        '
        Me.picAdd.BackgroundImage = CType(resources.GetObject("picAdd.BackgroundImage"), System.Drawing.Image)
        Me.picAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picAdd.Location = New System.Drawing.Point(422, 324)
        Me.picAdd.Name = "picAdd"
        Me.picAdd.Size = New System.Drawing.Size(78, 79)
        Me.picAdd.TabIndex = 4
        Me.picAdd.TabStop = False
        '
        'FormMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.picAdd)
        Me.Controls.Add(Me.picCheck)
        Me.Controls.Add(Me.DataGridViewMember)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FormMember"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FormMember"
        CType(Me.DataGridViewMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picAdd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents DataGridViewMember As DataGridView
    Friend WithEvents picCheck As PictureBox
    Friend WithEvents picAdd As PictureBox
End Class

﻿Imports System.Data.Odbc ' import namespaces ODBC class
Imports System.IO
Imports System.Data.OleDb
Imports iTextSharp.text ' import namespaces .net pdf library
Imports iTextSharp.text.pdf
Public Class BeliTiketOnline
    Dim judulTIket As String
    Private Sub BeliTiketOnline_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DataGridTampilPelangganONLINE.Hide()
        If DataGridTampilPelangganONLINE.Rows.Count <= 0 Then
            print.Enabled = False

        End If
        txtLokasiSave.Enabled = False
        If txtLokasiSave.Text = "" Then
            jikaLokasiKosong(False)
        End If
    End Sub
    Sub ReadData(query As String, kode As String)
        OPENKONEKSI()
        If New OleDbCommand(query, koneksi).ExecuteReader().HasRows = True Then
            DataGridTampilPelangganONLINE.Show()

            CMD = New OleDbCommand(query, koneksi)
            DTR = CMD.ExecuteReader
            DTR.Read()
            If DTR.HasRows Then
                OPENKONEKSI()
                DTA = New OleDbDataAdapter(query, koneksi)
                DTS = New DataSet
                DTA.Fill(DTS, "data_ketemu")
                DataGridTampilPelangganONLINE.DataSource = DTS.Tables("data_ketemu")
                DataGridTampilPelangganONLINE.ReadOnly = True
                print.Enabled = True

                SaveFileDialog1.FileName = ""
                SaveFileDialog1.Filter = "PDF (*.pdf)|*.pdf"
            End If
        Else
            DataGridTampilPelangganONLINE.Hide()
            MsgBox("Pelanggan Tidak Terdaftar!")
            DataGridTampilPelangganONLINE.Hide()
            print.Enabled = False
        End If
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.Filter = "PDF (*.pdf)|*.pdf"
    End Sub
    Private Sub cek()
        Dim kode As String
        kode = kd_pelanggan.Text
        If kode = "" Then
            MsgBox("Masukkan Kode!")
        Else

            Dim queryCari As String = "SELECT * FROM TRANSAKSI WHERE kd_transaksi='" & kode & "'"
            Dim queryCari2 As String = "select TRANSAKSI.kd_tiket, TRANSAKSI.nama, TRANSAKSI.kd_member,Tiket.Keterangan, TRANSAKSI.jml_tiket ,TIKET.Harga,((Transaksi.Potongan*Tiket.Harga)/100) AS [Potongan], Transaksi.totalBayar from TRANSAKSI,TIKET WHERE Transaksi.kd_tiket=Tiket.kd_tiket AND Transaksi.kd_transaksi='" & kode & "'"
            ReadData(queryCari2, kode)

        End If

    End Sub
    Private Sub btCekOnline_Click(sender As Object, e As EventArgs) Handles btCekOnline.Click
        cek()
    End Sub


    Private Sub kd_pelanggan_TextChanged(sender As Object, e As EventArgs) Handles kd_pelanggan.TextChanged

    End Sub
    Private Sub kd_pelanggan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles kd_pelanggan.KeyDown
        If e.KeyCode = Keys.Enter Then
            cek()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridTampilPelangganONLINE.CellContentClick

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
        MainMenu.Show()
    End Sub

    Private Sub btCetak_Click(sender As Object, e As EventArgs)


    End Sub


    Private Sub btBrowse_Click(sender As Object, e As EventArgs)
        SaveFileDialog1.FileName = ""
        If SaveFileDialog1.ShowDialog = DialogResult.OK Then
            ' declaration textbox2 to save file dialog name

        End If
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles print.Click
        ' you must import itextsharp namespace into our form
        ' download links is available in the descriptions
        Dim Paragraph As New Paragraph ' declaration for new paragraph
        Dim PdfFile As New Document(PageSize.A4, 40, 40, 40, 20) ' set pdf page size
        PdfFile.AddTitle(kd_pelanggan.Text) ' set our pdf title
        Dim Write As PdfWriter = PdfWriter.GetInstance(PdfFile, New FileStream(txtLokasiSave.Text & "ONLINE" + kd_pelanggan.Text & ".pdf", FileMode.Create))
        PdfFile.Open()

        ' declaration font type
        Dim pTitle As New Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim pTable As New Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        ' insert title into pdf file
        Paragraph = New Paragraph(New Chunk(kd_pelanggan.Text, pTitle))
        Paragraph.Alignment = Element.ALIGN_CENTER
        Paragraph.SpacingAfter = 5.0F

        ' set and add page with current settings
        PdfFile.Add(Paragraph)

        ' create data into table
        Dim PdfTable As New PdfPTable(DataGridTampilPelangganONLINE.Columns.Count)
        ' setting width of table
        PdfTable.TotalWidth = 500.0F
        PdfTable.LockedWidth = True

        Dim widths(0 To DataGridTampilPelangganONLINE.Columns.Count - 1) As Single
        For i As Integer = 0 To DataGridTampilPelangganONLINE.Columns.Count - 1
            widths(i) = 1.0F
        Next

        PdfTable.SetWidths(widths)
        PdfTable.HorizontalAlignment = 0
        PdfTable.SpacingBefore = 5.0F

        ' declaration pdf cells
        Dim pdfcell As PdfPCell = New PdfPCell

        ' create pdf header
        For i As Integer = 0 To DataGridTampilPelangganONLINE.Columns.Count - 1

            pdfcell = New PdfPCell(New Phrase(New Chunk(DataGridTampilPelangganONLINE.Columns(i).HeaderText, pTable)))
            ' alignment header table
            pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT
            ' add cells into pdf table
            PdfTable.AddCell(pdfcell)
        Next

        ' add data into pdf table
        For i As Integer = 0 To DataGridTampilPelangganONLINE.Rows.Count - 2

            For j As Integer = 0 To DataGridTampilPelangganONLINE.Columns.Count - 1
                pdfcell = New PdfPCell(New Phrase(DataGridTampilPelangganONLINE(j, i).Value.ToString(), pTable))
                PdfTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT
                PdfTable.AddCell(pdfcell)
            Next
        Next
        ' add pdf table into pdf document
        PdfFile.Add(PdfTable)
        PdfFile.Close() ' close all sessions

        ' show message if hasben exported
        Dim dialogResult As DialogResult
        dialogResult = MessageBox.Show("Tiket Berhasil Di Cetak !", "Informations", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If (dialogResult = DialogResult.OK) Then
            Process.Start("explorer.exe", String.Format("/n, /e, {0}", txtLokasiSave.Text & "ONLINE" + kd_pelanggan.Text & ".pdf"))
        Else

        End If
    End Sub

    Private Sub BbtBrowse_Click(sender As Object, e As EventArgs) Handles BbtBrowse.Click
        Dim folderDlg As New FolderBrowserDialog

        folderDlg.ShowNewFolderButton = True

        If (folderDlg.ShowDialog() = DialogResult.OK) Then

            txtLokasiSave.Text = folderDlg.SelectedPath + "\"
            If txtLokasiSave.Text <> "" Then
                jikaLokasiKosong(True)
            Else
                jikaLokasiKosong(False)
            End If

            Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If
    End Sub
    Private Sub jikaLokasiKosong(yn As Boolean)
        btCekOnline.Enabled = yn
        kd_pelanggan.Enabled = yn
        print.Enabled = yn

    End Sub

    Private Sub txtLokasiSave_TextChanged(sender As Object, e As EventArgs) Handles txtLokasiSave.TextChanged

    End Sub
End Class
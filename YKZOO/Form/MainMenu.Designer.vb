﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainMenu))
        Me.picMember = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.tiketoff = New System.Windows.Forms.PictureBox()
        Me.transaksi = New System.Windows.Forms.PictureBox()
        Me.logout = New System.Windows.Forms.PictureBox()
        Me.LabelAdmin = New System.Windows.Forms.Label()
        CType(Me.picMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tiketoff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.transaksi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.logout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picMember
        '
        Me.picMember.BackgroundImage = CType(resources.GetObject("picMember.BackgroundImage"), System.Drawing.Image)
        Me.picMember.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picMember.Location = New System.Drawing.Point(559, 161)
        Me.picMember.Name = "picMember"
        Me.picMember.Size = New System.Drawing.Size(132, 102)
        Me.picMember.TabIndex = 10
        Me.picMember.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(266, 161)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(116, 102)
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'tiketoff
        '
        Me.tiketoff.BackgroundImage = CType(resources.GetObject("tiketoff.BackgroundImage"), System.Drawing.Image)
        Me.tiketoff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tiketoff.Location = New System.Drawing.Point(105, 161)
        Me.tiketoff.Name = "tiketoff"
        Me.tiketoff.Size = New System.Drawing.Size(132, 102)
        Me.tiketoff.TabIndex = 8
        Me.tiketoff.TabStop = False
        '
        'transaksi
        '
        Me.transaksi.BackgroundImage = CType(resources.GetObject("transaksi.BackgroundImage"), System.Drawing.Image)
        Me.transaksi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.transaksi.Location = New System.Drawing.Point(427, 161)
        Me.transaksi.Name = "transaksi"
        Me.transaksi.Size = New System.Drawing.Size(105, 102)
        Me.transaksi.TabIndex = 6
        Me.transaksi.TabStop = False
        '
        'logout
        '
        Me.logout.BackgroundImage = CType(resources.GetObject("logout.BackgroundImage"), System.Drawing.Image)
        Me.logout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.logout.Location = New System.Drawing.Point(716, 367)
        Me.logout.Name = "logout"
        Me.logout.Size = New System.Drawing.Size(72, 71)
        Me.logout.TabIndex = 5
        Me.logout.TabStop = False
        '
        'LabelAdmin
        '
        Me.LabelAdmin.AutoSize = True
        Me.LabelAdmin.Location = New System.Drawing.Point(376, 76)
        Me.LabelAdmin.Name = "LabelAdmin"
        Me.LabelAdmin.Size = New System.Drawing.Size(54, 20)
        Me.LabelAdmin.TabIndex = 11
        Me.LabelAdmin.Text = "Admin"
        '
        'MainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.LabelAdmin)
        Me.Controls.Add(Me.picMember)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.tiketoff)
        Me.Controls.Add(Me.transaksi)
        Me.Controls.Add(Me.logout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "MainMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MainMenu"
        CType(Me.picMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tiketoff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.transaksi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.logout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents logout As PictureBox
    Friend WithEvents transaksi As PictureBox
    Friend WithEvents tiketoff As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents picMember As PictureBox
    Friend WithEvents LabelAdmin As Label
End Class

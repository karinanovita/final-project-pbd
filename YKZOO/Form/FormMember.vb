﻿Public Class FormMember
    Dim baris As Integer
    Public kodeMember, namaMember As String
    Public usia As Integer
    Private Sub FormMember_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadGrid()

    End Sub
    Private Sub LoadGrid()
        DTT = KontrolMember.selectMember.ToTable
        DataGridViewMember.DataSource = DTT
        If DTT.Rows.Count > 0 Then
            baris = DTT.Rows.Count - 1
            DataGridViewMember.Rows(DTT.Rows.Count - 1).Selected = True
            DataGridViewMember.CurrentCell = DataGridViewMember.Item(1, baris)

        End If
    End Sub

    Private Sub btPilih_Click(sender As Object, e As EventArgs)


    End Sub
    Public Sub sendMember(br As Integer)

        If br < DTT.Rows.Count Then
            With DataGridViewMember.Rows(br)
                kodeMember = .Cells(0).Value.ToString
                namaMember = .Cells(1).Value.ToString
                usia = .Cells(3).Value

            End With
        End If

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles picCheck.Click
        Me.Close()
        Belitiket.TxtKodeMember.Text = kodeMember
        Belitiket.TxtKodeMember.Enabled = False
        Belitiket.TxtNama.Text = namaMember
        Belitiket.TxtNama.Enabled = False
        Belitiket.TxtUsia.Text = usia
        Belitiket.TxtUsia.Enabled = False

    End Sub

    Private Sub picAdd_Click(sender As Object, e As EventArgs) Handles picAdd.Click
        AddMember.Show()

    End Sub

    Private Sub DataGridViewMember_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewMember.CellContentClick
        Dim newBaris As Integer
        newBaris = e.RowIndex
        DataGridViewMember.Rows(newBaris).Selected = True
        sendMember(newBaris)
    End Sub
End Class
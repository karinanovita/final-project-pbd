﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddMember
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtKodeMember = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtUsia = New System.Windows.Forms.TextBox()
        Me.TxtNama = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.picCetak = New System.Windows.Forms.PictureBox()
        Me.cbJenkel = New System.Windows.Forms.ComboBox()
        CType(Me.picCetak, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TxtKodeMember
        '
        Me.TxtKodeMember.Location = New System.Drawing.Point(428, 167)
        Me.TxtKodeMember.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtKodeMember.Name = "TxtKodeMember"
        Me.TxtKodeMember.Size = New System.Drawing.Size(225, 26)
        Me.TxtKodeMember.TabIndex = 64
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(317, 169)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 20)
        Me.Label5.TabIndex = 63
        Me.Label5.Text = "Kode Member"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(369, 204)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 20)
        Me.Label1.TabIndex = 62
        Me.Label1.Text = "Nama"
        '
        'TxtUsia
        '
        Me.TxtUsia.Location = New System.Drawing.Point(428, 239)
        Me.TxtUsia.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtUsia.Name = "TxtUsia"
        Me.TxtUsia.Size = New System.Drawing.Size(225, 26)
        Me.TxtUsia.TabIndex = 58
        '
        'TxtNama
        '
        Me.TxtNama.Location = New System.Drawing.Point(428, 203)
        Me.TxtNama.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TxtNama.Name = "TxtNama"
        Me.TxtNama.Size = New System.Drawing.Size(225, 26)
        Me.TxtNama.TabIndex = 57
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(379, 239)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 20)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Usia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(314, 284)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 20)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "Jenis Kelamin"
        '
        'picCetak
        '
        Me.picCetak.BackgroundImage = Global.YKZOO.My.Resources.Resources.simpannnn
        Me.picCetak.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picCetak.Location = New System.Drawing.Point(507, 342)
        Me.picCetak.Name = "picCetak"
        Me.picCetak.Size = New System.Drawing.Size(54, 54)
        Me.picCetak.TabIndex = 66
        Me.picCetak.TabStop = False
        '
        'cbJenkel
        '
        Me.cbJenkel.FormattingEnabled = True
        Me.cbJenkel.Location = New System.Drawing.Point(428, 284)
        Me.cbJenkel.Name = "cbJenkel"
        Me.cbJenkel.Size = New System.Drawing.Size(225, 28)
        Me.cbJenkel.TabIndex = 68
        '
        'AddMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(1050, 643)
        Me.Controls.Add(Me.cbJenkel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.picCetak)
        Me.Controls.Add(Me.TxtKodeMember)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtUsia)
        Me.Controls.Add(Me.TxtNama)
        Me.Controls.Add(Me.Label3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "AddMember"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AddMember"
        CType(Me.picCetak, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picCetak As PictureBox
    Friend WithEvents TxtKodeMember As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TxtUsia As TextBox
    Friend WithEvents TxtNama As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cbJenkel As ComboBox
End Class

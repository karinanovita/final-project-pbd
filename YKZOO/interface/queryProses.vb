﻿Imports System.Data.OleDb

Public Interface queryProses
    Function InsertTransaksi(Ob As Object) As OleDbCommand
    Function UpdateTransaksi(ob As Object) As OleDbCommand
    Function DeleteTransaksi(Ob As String) As OleDbCommand
    Function ReadTransaksi(kode As String) As DataView
    Function SearchTransaksi(kode As String) As DataView

End Interface

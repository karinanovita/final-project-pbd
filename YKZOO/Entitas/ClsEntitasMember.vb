﻿Public Class ClsEntitasMember
    Private kd_member As String
    Private nama_member As String
    Private jenis_kelamin As String
    Private usia As Integer

    Public Property Kd_member1 As String
        Get
            Return kd_member
        End Get
        Set(value As String)
            kd_member = value
        End Set
    End Property

    Public Property Nama_member1 As String
        Get
            Return nama_member
        End Get
        Set(value As String)
            nama_member = value
        End Set
    End Property

    Public Property Jenis_kelamin1 As String
        Get
            Return jenis_kelamin
        End Get
        Set(value As String)
            jenis_kelamin = value
        End Set
    End Property

    Public Property Usia1 As Integer
        Get
            Return usia
        End Get
        Set(value As Integer)
            usia = value
        End Set
    End Property
End Class

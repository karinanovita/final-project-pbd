﻿Public Class ClassTransaksi
    Private kd_transaksi As String
    Private kd_member As String
    Private nama As String
    Private kd_tiket As String
    Private tanggal As String
    Private jml_tiket As Integer
    Private username As String

    Public Property kodeTransaksi() As String
        Get
            Return kd_transaksi
        End Get
        Set(value As String)
            kd_transaksi = value
        End Set
    End Property
    Public Property kodeMember() As String
        Get
            Return kd_member
        End Get
        Set(value As String)
            kd_member = value
        End Set
    End Property
    Public Property namaPelanggan() As String
        Get
            Return nama
        End Get
        Set(value As String)
            nama = value

        End Set
    End Property
    Public Property kodeTiket() As String
        Get
            Return kd_tiket
        End Get
        Set(value As String)
            kd_tiket = value
        End Set
    End Property
    Public Property tanggalTransaksi() As String
        Get
            Return tanggal
        End Get
        Set(value As String)
            tanggal = value
        End Set
    End Property
    Public Property jmlTiketTransaksi() As Integer
        Get
            Return jml_tiket
        End Get
        Set(value As Integer)
            jml_tiket = value
        End Set
    End Property

    Public Property Username1 As String
        Get
            Return username
        End Get
        Set(value As String)
            username = value
        End Set
    End Property
End Class

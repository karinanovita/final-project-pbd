﻿Imports YKZOO

Public Class ClsCtlLaporan : Implements laporanView

    Public Function showLaporanReguler() As DataView Implements laporanView.showLaporanReguler

        OPENKONEKSI()
        Try
            DTA = New OleDb.OleDbDataAdapter("SELECT COUNT(kd_transaksi) AS [JUMLAH PENGUNJUNG] , SUM(totalBayar) AS PENDAPATAN  FROM TRANSAKSI WHERE kd_tiket='REGUL'", OPENKONEKSI)
            DTS = New DataSet
            DTA.Fill(DTS, "table_reguler")
            Dim grid As New DataView(DTS.Tables("table_reguler"))
            Return grid
        Catch ex As Exception

            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function showLaporanPremium() As DataView Implements laporanView.showLaporanPremium

        OPENKONEKSI()
        Try
            DTA = New OleDb.OleDbDataAdapter("SELECT COUNT(kd_transaksi) AS [JUMLAH PENGUNJUNG] , SUM(totalBayar) AS PENDAPATAN  FROM TRANSAKSI WHERE kd_tiket='PREMI'", OPENKONEKSI)
            DTS = New DataSet
            DTA.Fill(DTS, "table_reguler")
            Dim grid As New DataView(DTS.Tables("table_reguler"))
            Return grid
        Catch ex As Exception

            Throw New Exception(ex.Message)
        End Try
    End Function
End Class

﻿Imports System.Data.OleDb
Imports YKZOO

Public Class CtrlMember : Implements memberView
    Public Function kodeMember() As String
        Dim baru As String
        Dim lastCode As Integer
        Try
            DTA = New OleDbDataAdapter("SELECT MAX(right(kd_member,6)) from MEMBER", OPENKONEKSI)
            DTS = New DataSet()
            DTA.Fill(DTS, "kd_member")
            lastCode = Val(DTS.Tables("kd_member").Rows(0).Item(0))
            baru = "MEM" & Strings.Right("000000" & lastCode + 1, 9)
            Return baru
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function selectMember() As DataView Implements memberView.selectMember
        Dim Data As New ClsEntitasMember
        Try
            DTA = New OleDbDataAdapter("SELECT * FROM MEMBER WHERE kd_member !='-'", OPENKONEKSI)
            DTS = New DataSet
            DTA.Fill(DTS, "tabel_member")
            Dim grid As New DataView(DTS.Tables("tabel_member"))
            Return grid

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function AddMember(Ob As Object) As OleDbCommand Implements memberView.AddMember
        Dim data As New ClsEntitasMember
        data = Ob
        Dim query As String = "INSERT INTO MEMBER VALUES ('" & data.Kd_member1 & "','" & data.Nama_member1 & "','" & data.Jenis_kelamin1 & "'," & data.Usia1 & ")"
        CMD = New OleDbCommand(query, OPENKONEKSI)
        CMD.CommandType = CommandType.Text
        CMD.ExecuteNonQuery()
        CMD = New OleDbCommand("", CLOSEKONEKSI)
        Return CMD
    End Function
End Class

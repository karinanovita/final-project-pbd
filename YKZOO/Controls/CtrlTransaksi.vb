﻿Imports System.Data.OleDb
Imports YKZOO

Public Class CtrlTransaksi : Implements queryProses
    Public Function kodeTransaksi() As String
        Dim baru As String
        Dim lastCode As Integer
        Try
            DTA = New OleDbDataAdapter("SELECT MAX(right(kd_transaksi,4)) from TRANSAKSI", OPENKONEKSI)
            DTS = New DataSet()
            DTA.Fill(DTS, "max_kode")
            lastCode = Val(DTS.Tables("max_kode").Rows(0).Item(0))
            baru = "T" & Strings.Right("000" & lastCode + 1, 4)
            Return baru
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function InsertTransaksi(Ob As Object) As OleDbCommand Implements queryProses.InsertTransaksi
        Dim data As New ClassTransaksi
        data = Ob
        Dim potongan As Double
        potongan = 0.2
        If Belitiket.TxtKodeMember.Text <> "" And data.kodeTiket = "REGUL" Then
            Dim TotalBayar1 As Integer
            TotalBayar1 = (25000 - (25000 * 20 / 100)) * data.jmlTiketTransaksi
            Dim query As String = "insert into TRANSAKSI values('" & data.kodeTransaksi & "','" & data.kodeMember & "','" & data.namaPelanggan & "','" & data.kodeTiket & "','" & data.tanggalTransaksi & "','" & data.jmlTiketTransaksi & "','" & 20 & "','" & TotalBayar1 & "','" & data.Username1 & "')"
            CMD = New OleDbCommand(query, OPENKONEKSI)
            CMD.CommandType = CommandType.Text
            CMD.ExecuteNonQuery()
            CMD = New OleDbCommand("", CLOSEKONEKSI)
            Return CMD
        ElseIf Belitiket.TxtKodeMember.Text <> "" And data.kodeTiket = "PREMI" Then
            Dim TotalBayar2 As Integer
            TotalBayar2 = (100000 - (100000 * 20 / 100)) * data.jmlTiketTransaksi
            Dim query As String = "insert into TRANSAKSI values('" & data.kodeTransaksi & "','" & data.kodeMember & "','" & data.namaPelanggan & "','" & data.kodeTiket & "','" & data.tanggalTransaksi & "','" & data.jmlTiketTransaksi & "','" & 20 & "','" & TotalBayar2 & "','" & data.Username1 & "')"
            CMD = New OleDbCommand(query, OPENKONEKSI)
            CMD.CommandType = CommandType.Text
            CMD.ExecuteNonQuery()
            CMD = New OleDbCommand("", CLOSEKONEKSI)
            Return CMD
        ElseIf Belitiket.TxtKodeMember.Text = "" And data.kodeTiket = "PREMI" Then
            Dim TotalBayar3 As Integer
            TotalBayar3 = (100000) * data.jmlTiketTransaksi
            Dim query As String = "insert into TRANSAKSI values('" & data.kodeTransaksi & "','" & "-" & "','" & data.namaPelanggan & "','" & data.kodeTiket & "','" & data.tanggalTransaksi & "','" & data.jmlTiketTransaksi & "','" & 0 & "','" & TotalBayar3 & "','" & data.Username1 & "')"
            CMD = New OleDbCommand(query, OPENKONEKSI)
            CMD.CommandType = CommandType.Text
            CMD.ExecuteNonQuery()
            CMD = New OleDbCommand("", CLOSEKONEKSI)

            Return CMD
        ElseIf Belitiket.TxtKodeMember.Text = "" And data.kodeTiket = "REGUL" Then
            Dim TotalBayar4 As Integer
            TotalBayar4 = (25000) * data.jmlTiketTransaksi
            Dim query As String = "insert into TRANSAKSI values('" & data.kodeTransaksi & "','" & "-" & "','" & data.namaPelanggan & "','" & data.kodeTiket & "','" & data.tanggalTransaksi & "','" & data.jmlTiketTransaksi & "','" & 0 & "','" & TotalBayar4 & "','" & data.Username1 & "')"
            CMD = New OleDbCommand(query, OPENKONEKSI)
            CMD.CommandType = CommandType.Text
            CMD.ExecuteNonQuery()
            CMD = New OleDbCommand("", CLOSEKONEKSI)
        End If


    End Function

    Public Function UpdateTransaksi(ob As Object) As OleDbCommand Implements queryProses.UpdateTransaksi
        Throw New NotImplementedException()
    End Function

    Public Function DeleteTransaksi(Ob As String) As OleDbCommand Implements queryProses.DeleteTransaksi
        Throw New NotImplementedException()
    End Function

    Public Function ReadTransaksi(kode As String) As DataView Implements queryProses.ReadTransaksi
        Dim data As New ClassTransaksi
        Try
            DTA = New OleDbDataAdapter("select TRANSAKSI.kd_tiket, TRANSAKSI.nama, TRANSAKSI.kd_member, TRANSAKSI.jml_tiket ,Tiket.Keterangan AS FASILITAS,TIKET.Harga,((Transaksi.Potongan*Tiket.Harga)/100) AS [Potongan], Transaksi.totalBayar ,Transaksi.username AS [Admin] from TRANSAKSI,TIKET WHERE Transaksi.kd_tiket=Tiket.kd_tiket AND Transaksi.kd_transaksi='" & kode & "'", OPENKONEKSI)

            DTS = New DataSet()
            DTA.Fill(DTS, "tabel_transaksi")
            Dim grid As New DataView(DTS.Tables("tabel_transaksi"))

            Return grid

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function SearchTransaksi(kode As String) As DataView Implements queryProses.SearchTransaksi
        Throw New NotImplementedException()
    End Function
End Class

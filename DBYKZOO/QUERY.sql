CREATE DATABASE DBYKZOO ON
(NAME=ykzoo_dat,
FILENAME='F:\PBD\DBYKZOO\ykzoo.mdf',
MAXSIZE=50,
SIZE=25,
FILEGROWTH=1
)
LOG ON
(NAME=ykzoo_log,
FILENAME='F:\PBD\DBYKZOO\ykzoo.ldf',
SIZE=25,MAXSIZE=50,
FILEGROWTH=1
)
USE DBYKZOO

CREATE TABLE MEMBER (
kd_member char(10) primary key not null,
nama_member varchar(100) not null,
jenis_kelamin char(1) not null,
usia int not null
)
INSERT INTO MEMBER VALUES
('-','NON MEMBER','-','-'),
('MEM0000001','PAIJO STREET','L','20')
CREATE TABLE JENIS_KARYAWAN(
kd_jenkar char(5) primary key not null,
nama_jenkar varchar(20) not null,
gaji int not null
)
CREATE TABLE KARYAWAN(
kd_karyawan char(5) primary key not null,
nama_karyawan varchar(100) not null,
kd_jenkar char(5) foreign key references JENIS_KARYAWAN(kd_jenkar) on UPDATE CASCADE on DELETE CASCADE not null,
jenis_kelamin char(1) not null,
alamat varchar(100) not null,
no_hp numeric not null
)
CREATE TABLE TIKET(
kd_tiket char(5) primary key not null,
nama_tiket varchar(10) not null,
keterangan varchar(100) not null,
harga int not null
)
INSERT INTO TIKET VALUES
('REGUL','REGULER','-',25000),
('PREMI','PREMIUM','Free Snack Box , Wifi , Speed Boat , Aquarium Raksasa , Guide',100000)
INSERT INTO MEMBER VALUES
()
CREATE TABLE TRANSAKSI(
kd_transaksi char (5) primary key not null,
kd_member char(10) foreign key references MEMBER(kd_member),
nama varchar(100),
kd_tiket char(5) foreign key references TIKET(kd_tiket) on UPDATE CASCADE on DELETE CASCADE not null,
tanggal date ,
jml_tiket int not null
)
DROP TABLE TRANSAKSI
CREATE TABLE ADMIN (
username varchar(100) primary key not null,
passwords varchar(100) not null)
INSERT INTO TRANSAKSI VALUES
('K0001','-','Sutanto','REGUL','2019-06-17',2)
DELETE TRANSAKSI WHERE kd_transaksi = 'T0001'
DROP TABLE ADMIN
INSERT INTO ADMIN VALUES
('gustumaulanaf','gustuamikom')
